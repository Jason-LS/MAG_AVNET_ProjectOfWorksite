<?php

namespace App\Entity;

use App\Repository\WorksiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: WorksiteRepository::class)]
class Worksite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 20)]
    #[Assert\NotBlank]
    private ?string $code4D = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank]
    private ?string $address = null;

    #[ORM\Column(length: 10)]
    #[Assert\NotBlank]
    private ?string $zipcode = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $city = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\DateTime]
    private ?\DateTimeInterface $start = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\DateTime]
    private ?\DateTimeInterface $end = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\DateTime]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\DateTime]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\OneToMany(mappedBy: 'worksite', targetEntity: Mission::class)]
    private Collection $missionsWorksite;

    #[ORM\OneToMany(mappedBy: 'worksite', targetEntity: Note::class)]
    private Collection $notesWorksite;

    #[ORM\ManyToMany(targetEntity: SiteManager::class, mappedBy: 'worksiteManager')]
    private Collection $siteManagers;

    public function __construct()
    {
        $this->missionsWorksite = new ArrayCollection();
        $this->notesWorksite = new ArrayCollection();
        $this->siteManagers = new ArrayCollection();

        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCode4D(): ?string
    {
        return $this->code4D;
    }

    public function setCode4D(string $code4D): self
    {
        $this->code4D = $code4D;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
    
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }
    
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, Mission>
     */
    public function getMissionsWorksite(): Collection
    {
        return $this->missionsWorksite;
    }

    public function addMissionsWorksite(Mission $missionsWorksite): self
    {
        if (!$this->missionsWorksite->contains($missionsWorksite)) {
            $this->missionsWorksite->add($missionsWorksite);
            $missionsWorksite->setWorksite($this);
        }

        return $this;
    }

    public function removeMissionsWorksite(Mission $missionsWorksite): self
    {
        if ($this->missionsWorksite->removeElement($missionsWorksite)) {
            // set the owning side to null (unless already changed)
            if ($missionsWorksite->getWorksite() === $this) {
                $missionsWorksite->setWorksite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Note>
     */
    public function getNotesWorksite(): Collection
    {
        return $this->notesWorksite;
    }

    public function addNotesWorksite(Note $notesWorksite): self
    {
        if (!$this->notesWorksite->contains($notesWorksite)) {
            $this->notesWorksite->add($notesWorksite);
            $notesWorksite->setWorksite($this);
        }

        return $this;
    }

    public function removeNotesWorksite(Note $notesWorksite): self
    {
        if ($this->notesWorksite->removeElement($notesWorksite)) {
            // set the owning side to null (unless already changed)
            if ($notesWorksite->getWorksite() === $this) {
                $notesWorksite->setWorksite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SiteManager>
     */
    public function getSiteManagers(): Collection
    {
        return $this->siteManagers;
    }

    public function addSiteManager(SiteManager $siteManager): self
    {
        if (!$this->siteManagers->contains($siteManager)) {
            $this->siteManagers->add($siteManager);
            $siteManager->addWorksiteManager($this);
        }

        return $this;
    }

    public function removeSiteManager(SiteManager $siteManager): self
    {
        if ($this->siteManagers->removeElement($siteManager)) {
            $siteManager->removeWorksiteManager($this);
        }

        return $this;
    }
}
