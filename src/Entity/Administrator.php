<?php

namespace App\Entity;

use App\Repository\AdministratorRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdministratorRepository::class)]
class Administrator
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'administrators')]
    private ?User $userAdministrator = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserAdministrator(): ?User
    {
        return $this->userAdministrator;
    }

    public function setUserAdministrator(?User $userAdministrator): self
    {
        $this->userAdministrator = $userAdministrator;

        return $this;
    }
}
