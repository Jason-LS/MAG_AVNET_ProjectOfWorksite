<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Symfony\Component\Uid\Uuid;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity('email','Cet email existe déjà au sein de cette application !')]
class User
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $firstname = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $lastname = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\Email()]
    private ?string $email = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Assert\Date]
    private ?\DateTimeInterface $birthday = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\DateTime]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\DateTime]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $address = null;

    #[ORM\Column(length: 10, nullable: true)]
    private ?string $zipcode = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $city = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotCompromisedPassword]
    private ?string $password = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Note::class)]
    private Collection $notesUser;

    #[ORM\ManyToMany(targetEntity: Skill::class, inversedBy: 'users')]
    private Collection $skillsUser;

    #[ORM\ManyToOne(inversedBy: 'users')]
    private ?TechnicalCenter $technicalCenterUser = null;

    #[ORM\OneToMany(mappedBy: 'userAdministrator', targetEntity: Administrator::class)]
    private Collection $administrators;

    #[ORM\OneToMany(mappedBy: 'userManager', targetEntity: SiteManager::class)]
    private Collection $siteManagers;

    #[ORM\OneToMany(mappedBy: 'userTechnician', targetEntity: Technician::class)]
    private Collection $technicians;

    #[ORM\OneToMany(mappedBy: 'userComment', targetEntity: Comment::class)]
    private Collection $comments;

    public function __construct()
    {
        $this->notesUser = new ArrayCollection();
        $this->skillsUser = new ArrayCollection();
        $this->administrators = new ArrayCollection();
        $this->siteManagers = new ArrayCollection();
        $this->technicians = new ArrayCollection();
        $this->comments = new ArrayCollection();

        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }
 
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
    
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }
    
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(?string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection<int, Note>
     */
    public function getNotesUser(): Collection
    {
        return $this->notesUser;
    }

    public function addNotesUser(Note $notesUser): self
    {
        if (!$this->notesUser->contains($notesUser)) {
            $this->notesUser->add($notesUser);
            $notesUser->setUser($this);
        }

        return $this;
    }

    public function removeNotesUser(Note $notesUser): self
    {
        if ($this->notesUser->removeElement($notesUser)) {
            // set the owning side to null (unless already changed)
            if ($notesUser->getUser() === $this) {
                $notesUser->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Skill>
     */
    public function getSkillsUser(): Collection
    {
        return $this->skillsUser;
    }

    public function addSkillsUser(Skill $skillsUser): self
    {
        if (!$this->skillsUser->contains($skillsUser)) {
            $this->skillsUser->add($skillsUser);
        }

        return $this;
    }

    public function removeSkillsUser(Skill $skillsUser): self
    {
        $this->skillsUser->removeElement($skillsUser);

        return $this;
    }

    public function getTechnicalCenterUser(): ?TechnicalCenter
    {
        return $this->technicalCenterUser;
    }

    public function setTechnicalCenterUser(?TechnicalCenter $technicalCenterUser): self
    {
        $this->technicalCenterUser = $technicalCenterUser;

        return $this;
    }

    /**
     * @return Collection<int, Administrator>
     */
    public function getAdministrators(): Collection
    {
        return $this->administrators;
    }

    public function addAdministrator(Administrator $administrator): self
    {
        if (!$this->administrators->contains($administrator)) {
            $this->administrators->add($administrator);
            $administrator->setUserAdministrator($this);
        }

        return $this;
    }

    public function removeAdministrator(Administrator $administrator): self
    {
        if ($this->administrators->removeElement($administrator)) {
            // set the owning side to null (unless already changed)
            if ($administrator->getUserAdministrator() === $this) {
                $administrator->setUserAdministrator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SiteManager>
     */
    public function getSiteManagers(): Collection
    {
        return $this->siteManagers;
    }

    public function addSiteManager(SiteManager $siteManager): self
    {
        if (!$this->siteManagers->contains($siteManager)) {
            $this->siteManagers->add($siteManager);
            $siteManager->setUserManager($this);
        }

        return $this;
    }

    public function removeSiteManager(SiteManager $siteManager): self
    {
        if ($this->siteManagers->removeElement($siteManager)) {
            // set the owning side to null (unless already changed)
            if ($siteManager->getUserManager() === $this) {
                $siteManager->setUserManager(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Technician>
     */
    public function getTechnicians(): Collection
    {
        return $this->technicians;
    }

    public function addTechnician(Technician $technician): self
    {
        if (!$this->technicians->contains($technician)) {
            $this->technicians->add($technician);
            $technician->setUserTechnician($this);
        }

        return $this;
    }

    public function removeTechnician(Technician $technician): self
    {
        if ($this->technicians->removeElement($technician)) {
            // set the owning side to null (unless already changed)
            if ($technician->getUserTechnician() === $this) {
                $technician->setUserTechnician(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setUserComment($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUserComment() === $this) {
                $comment->setUserComment(null);
            }
        }

        return $this;
    }

}
