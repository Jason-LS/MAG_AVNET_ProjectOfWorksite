<?php

namespace App\Entity;

use App\Repository\TechnicianRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TechnicianRepository::class)]
class Technician
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'technicians')]
    private ?User $userTechnician = null;

    #[ORM\ManyToMany(targetEntity: Task::class, inversedBy: 'technicians')]
    private Collection $technicianProject;

    public function __construct()
    {
        $this->technicianProject = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserTechnician(): ?User
    {
        return $this->userTechnician;
    }

    public function setUserTechnician(?User $userTechnician): self
    {
        $this->userTechnician = $userTechnician;

        return $this;
    }

    /**
     * @return Collection<int, Task>
     */
    public function getTechnicianProject(): Collection
    {
        return $this->technicianProject;
    }

    public function addTechnicianProject(Task $technicianProject): self
    {
        if (!$this->technicianProject->contains($technicianProject)) {
            $this->technicianProject->add($technicianProject);
        }

        return $this;
    }

    public function removeTechnicianProject(Task $technicianProject): self
    {
        $this->technicianProject->removeElement($technicianProject);

        return $this;
    }
}
