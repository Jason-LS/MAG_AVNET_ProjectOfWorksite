<?php

namespace App\Entity;

use App\Repository\SiteManagerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SiteManagerRepository::class)]
class SiteManager
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'siteManagers')]
    private ?User $userManager = null;

    #[ORM\ManyToMany(targetEntity: Worksite::class, inversedBy: 'siteManagers')]
    private Collection $worksiteManager;

    public function __construct()
    {
        $this->worksiteManager = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserManager(): ?User
    {
        return $this->userManager;
    }

    public function setUserManager(?User $userManager): self
    {
        $this->userManager = $userManager;

        return $this;
    }

    /**
     * @return Collection<int, Worksite>
     */
    public function getWorksiteManager(): Collection
    {
        return $this->worksiteManager;
    }

    public function addWorksiteManager(Worksite $worksiteManager): self
    {
        if (!$this->worksiteManager->contains($worksiteManager)) {
            $this->worksiteManager->add($worksiteManager);
        }

        return $this;
    }

    public function removeWorksiteManager(Worksite $worksiteManager): self
    {
        $this->worksiteManager->removeElement($worksiteManager);

        return $this;
    }
}
