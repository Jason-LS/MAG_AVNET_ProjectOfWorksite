<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230410204809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment ADD created_at DATE NOT NULL, ADD updated_at DATE NOT NULL, DROP date');
        $this->addSql('ALTER TABLE note ADD created_at DATE NOT NULL, ADD updated_at DATE NOT NULL, DROP date');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment ADD date DATETIME NOT NULL, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE note ADD date DATETIME NOT NULL, DROP created_at, DROP updated_at');
    }
}
