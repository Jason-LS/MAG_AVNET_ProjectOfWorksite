<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230410203106 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE administrator (id INT AUTO_INCREMENT NOT NULL, user_administrator_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_58DF0651361CABF1 (user_administrator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, user_comment_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', note_comment_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, description LONGTEXT NOT NULL, date DATETIME NOT NULL, INDEX IDX_9474526C5F0EBBFF (user_comment_id), INDEX IDX_9474526C98CE5423 (note_comment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mission (id INT AUTO_INCREMENT NOT NULL, worksite_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, duration VARCHAR(255) DEFAULT NULL, start DATETIME DEFAULT NULL, end DATETIME DEFAULT NULL, INDEX IDX_9067F23CA47737E7 (worksite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE note (id INT AUTO_INCREMENT NOT NULL, worksite_id INT DEFAULT NULL, user_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(255) DEFAULT NULL, description LONGTEXT NOT NULL, date DATETIME NOT NULL, INDEX IDX_CFBDFA14A47737E7 (worksite_id), INDEX IDX_CFBDFA14A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_manager (id INT AUTO_INCREMENT NOT NULL, user_manager_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_180C772DF59F28F (user_manager_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_manager_worksite (site_manager_id INT NOT NULL, worksite_id INT NOT NULL, INDEX IDX_9DFA9EC85837BBA6 (site_manager_id), INDEX IDX_9DFA9EC8A47737E7 (worksite_id), PRIMARY KEY(site_manager_id, worksite_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE skill (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, start DATE DEFAULT NULL, end DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task (id INT AUTO_INCREMENT NOT NULL, mission_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, start DATETIME DEFAULT NULL, end DATETIME DEFAULT NULL, duration VARCHAR(255) DEFAULT NULL, INDEX IDX_527EDB25BE6CAE90 (mission_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE technical_center (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, start DATE DEFAULT NULL, end DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE technician (id INT AUTO_INCREMENT NOT NULL, user_technician_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_F244E94884B79C6 (user_technician_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE technician_task (technician_id INT NOT NULL, task_id INT NOT NULL, INDEX IDX_9DB61027E6C5D496 (technician_id), INDEX IDX_9DB610278DB60186 (task_id), PRIMARY KEY(technician_id, task_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', technical_center_user_id INT DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, birthday DATE DEFAULT NULL, created_at DATE DEFAULT NULL, updated_at DATE DEFAULT NULL, address LONGTEXT DEFAULT NULL, zipcode VARCHAR(10) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, INDEX IDX_8D93D649C1FCD443 (technical_center_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_skill (user_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', skill_id INT NOT NULL, INDEX IDX_BCFF1F2FA76ED395 (user_id), INDEX IDX_BCFF1F2F5585C142 (skill_id), PRIMARY KEY(user_id, skill_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE worksite (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, code4_d VARCHAR(20) NOT NULL, address LONGTEXT NOT NULL, zipcode VARCHAR(10) NOT NULL, city VARCHAR(255) NOT NULL, start DATETIME NOT NULL, end DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE administrator ADD CONSTRAINT FK_58DF0651361CABF1 FOREIGN KEY (user_administrator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C5F0EBBFF FOREIGN KEY (user_comment_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C98CE5423 FOREIGN KEY (note_comment_id) REFERENCES note (id)');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23CA47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14A47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE site_manager ADD CONSTRAINT FK_180C772DF59F28F FOREIGN KEY (user_manager_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE site_manager_worksite ADD CONSTRAINT FK_9DFA9EC85837BBA6 FOREIGN KEY (site_manager_id) REFERENCES site_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_manager_worksite ADD CONSTRAINT FK_9DFA9EC8A47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25BE6CAE90 FOREIGN KEY (mission_id) REFERENCES mission (id)');
        $this->addSql('ALTER TABLE technician ADD CONSTRAINT FK_F244E94884B79C6 FOREIGN KEY (user_technician_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE technician_task ADD CONSTRAINT FK_9DB61027E6C5D496 FOREIGN KEY (technician_id) REFERENCES technician (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE technician_task ADD CONSTRAINT FK_9DB610278DB60186 FOREIGN KEY (task_id) REFERENCES task (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649C1FCD443 FOREIGN KEY (technical_center_user_id) REFERENCES technical_center (id)');
        $this->addSql('ALTER TABLE user_skill ADD CONSTRAINT FK_BCFF1F2FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_skill ADD CONSTRAINT FK_BCFF1F2F5585C142 FOREIGN KEY (skill_id) REFERENCES skill (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE administrator DROP FOREIGN KEY FK_58DF0651361CABF1');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C5F0EBBFF');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C98CE5423');
        $this->addSql('ALTER TABLE mission DROP FOREIGN KEY FK_9067F23CA47737E7');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14A47737E7');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14A76ED395');
        $this->addSql('ALTER TABLE site_manager DROP FOREIGN KEY FK_180C772DF59F28F');
        $this->addSql('ALTER TABLE site_manager_worksite DROP FOREIGN KEY FK_9DFA9EC85837BBA6');
        $this->addSql('ALTER TABLE site_manager_worksite DROP FOREIGN KEY FK_9DFA9EC8A47737E7');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25BE6CAE90');
        $this->addSql('ALTER TABLE technician DROP FOREIGN KEY FK_F244E94884B79C6');
        $this->addSql('ALTER TABLE technician_task DROP FOREIGN KEY FK_9DB61027E6C5D496');
        $this->addSql('ALTER TABLE technician_task DROP FOREIGN KEY FK_9DB610278DB60186');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649C1FCD443');
        $this->addSql('ALTER TABLE user_skill DROP FOREIGN KEY FK_BCFF1F2FA76ED395');
        $this->addSql('ALTER TABLE user_skill DROP FOREIGN KEY FK_BCFF1F2F5585C142');
        $this->addSql('DROP TABLE administrator');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE mission');
        $this->addSql('DROP TABLE note');
        $this->addSql('DROP TABLE site_manager');
        $this->addSql('DROP TABLE site_manager_worksite');
        $this->addSql('DROP TABLE skill');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE technical_center');
        $this->addSql('DROP TABLE technician');
        $this->addSql('DROP TABLE technician_task');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_skill');
        $this->addSql('DROP TABLE worksite');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
